import {useState,useContext,useEffect} from 'react'

import Banner from '../components/Banner';
import CourseCard from '../components/Course';
//import coursesData from '../data/courses';

import AdminDashboard from '../components/AdminDashboard'

import UserContext from '../userContext'

export default function Courses(){


	const {user} = useContext(UserContext)
	//console.log(user)
	//console.log(coursesData)
	let coursesBanner ={
		title: "Welcome to the Courses Page.",
		description: "View one of our courses below.",
		buttonText: "Register/Login to Enroll",
		destination: "/register"
	}

	//let propCourse1=coursesData[0];
	//let propCourse2="sample data 2";
	//let propCourse3="sample data 3";

	const [coursesArray,setCoursesArray] = useState([])

	useEffect(()=>{

		fetch("http://localhost:4000/courses/getActiveCourses")
		.then(res => res.json())
		.then(data => {

			setCoursesArray(data.map(course =>{

				return (

					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})

	},[])

	//console.log(coursesArray)

	return(

		user.isAdmin
		? <AdminDashboard />
		:

		<>
			<Banner bannerProp={coursesBanner}/>
			{coursesArray}

		</>
	)
}