import Banner from '../components/Banner';
import	Highlights from '../components/Highlights';

export default function Home(){

	//let sampleProp = "I am a sample data passed from Home component to Banner component."
	let sampleProp2 = "I am a sample data passed from Home component to Highlights component."

	let bannerData = {
		title: "Zuitt Booking System B152",
		description: "View and book a course from our catalog!",
		buttonText: "View Our Courses",
		destination: "/courses"
	}

	return (
		<>
			<Banner bannerProp={bannerData}/>
			<Highlights bannerProp={sampleProp2}/>
		</>

		)
}