let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum reprehenderit consequat in ea amet anim commodo nostrud pariatur fugiat veniam minim proident enim est proident laboris amet pariatur.",
		price: 25000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "In occaecat est nisi enim fugiat incididunt fugiat elit cupidatat est.",
		price: 35000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum sit nulla deserunt aute dolore aute ut magna voluptate nisi velit ut reprehenderit exercitation anim dolore nisi aute veniam.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "Java-Rice",
		description: "Enim aliquip cillum ea proident aliqua elit anim dolore amet exercitation enim ad deserunt officia dolor esse irure.",
		price: 15,
		onOffer: false
	}

]

export default coursesData;