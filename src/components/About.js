import {Row, Col, Container} from 'react-bootstrap'

export default function About(){
	return(
<Container className="bg-primary col-12 col-md-6 text-center jumbotron">
	<Row>
		<Col className="p-5">
			<h1 className="mt-3 mb-5">About Me</h1>
			<h2 className="mt-3">Angelo Zantua</h2>
			<h3>Full Stack Web Developer</h3>
			<p className="mb-4">hello</p>
			<h4>Contact Me</h4>
			<ul>
				<li>Email</li>
				<li>Mobile Number</li>
				<li>Address</li>
			</ul>
		</Col>
	</Row>
</Container>

)}