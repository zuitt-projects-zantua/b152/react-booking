import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights({bannerProp}){

	console.log(bannerProp)

	return(
		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="border-primary p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Veniam ea laboris officia mollit proident eiusmod sit consectetur dolore et eu occaecat exercitation in minim tempor laboris dolore.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="border-primary p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Ea pariatur quis aliquip et est non fugiat ut in do ea aute id dolore et nisi cupidatat elit tempor dolore enim laboris eu tempor ad minim anim occaecat esse mollit dolor.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="border-primary p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Elit enim anim consectetur proident in consequat nulla cillum proident esse reprehenderit fugiat mollit. Voluptate nostrud magna et voluptate nostrud dolore ex exercitation tempor.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		)
}